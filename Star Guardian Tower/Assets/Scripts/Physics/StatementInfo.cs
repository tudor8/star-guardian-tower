﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * These are basically used by the rays, they have the same effect as
 * continue and break from inside a loop.
 */

public enum StatementInfo {
	Continue, Break
}