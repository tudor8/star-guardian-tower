﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Holds a reference to the editor script for a room segment.
 */

public class PointRefs : MonoBehaviour {
	public RoomSegmentHelper roomEditor;
}
