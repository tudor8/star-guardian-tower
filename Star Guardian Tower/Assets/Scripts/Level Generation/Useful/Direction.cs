﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Directions used by room entry and exit points.
 */
public enum Direction {
	None, Top, Bottom, Left, Right
}