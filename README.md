# Procedural Tree Generator

Repository for Star Guardian Tower.

# Getting Started

This game is a custom physics engine made for a 2D platformer.

It was primarily  made for the Rising Star competition.

Unity Editor scripts are used for level creation, which is then stored in a cache that selects random rooms during play. 

The Camera is bound inside the rooms to give a roguelike feeling. 

# Running

In the executable folder you can find a runnable version of the application.
 
# Built With

* [Unity](https://unity.com/) - Main Engine

# Authors

* **Tudor Gheorghe** - (http://www.tudorgheorghe.net)